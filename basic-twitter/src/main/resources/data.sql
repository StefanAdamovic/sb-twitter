-- Tweet 1
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 1', 'user1', '2023-12-23 16:37:27');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('1', 'tag1'), ('1', 'tag2'), ('1', 'tag3');

-- Tweet 2
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 2', 'user2', '2023-12-23 17:45:10');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('2', 'tag4'), ('2', 'tag5'), ('2', 'tag6');

-- Tweet 3
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 3', 'user3', '2023-12-23 18:20:45');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('3', 'tag7'), ('3', 'tag8'), ('3', 'tag9');

-- Tweet 4
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 4', 'user4', '2023-12-23 19:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('4', 'tag10');

-- Tweet 5
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 5', 'user1', '2023-12-23 22:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('5', 'tag1'), ('5', 'tag2'), ('5', 'tag3'), ('5', 'tag4'), ('5', 'tag5');

-- Tweet 6
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 6', 'user2', '2023-12-23 22:30:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('6', 'tag6'), ('6', 'tag7'), ('6', 'tag8'), ('6', 'tag9'), ('6', 'tag10');

-- Tweet 7
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 7', 'user3', '2023-12-23 23:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('7', 'tag1'), ('7', 'tag3'), ('7', 'tag5');

-- Tweet 8
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 8', 'user4', '2023-12-23 23:30:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('8', 'tag7'), ('8', 'tag9'), ('8', 'tag10');

-- Tweet 9
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 9', 'user5', '2023-12-24 00:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('9', 'tag1'), ('9', 'tag2');

-- Tweet 10
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 10', 'user1', '2023-12-24 01:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('10', 'tag3'), ('10', 'tag4');

-- Tweet 11
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 11', 'user2', '2023-12-24 02:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('11', 'tag5'), ('11', 'tag6'), ('11', 'tag8'), ('11', 'tag9'), ('11', 'tag10');

-- Tweet 12
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 12', 'user3', '2023-12-24 03:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('12', 'tag2'), ('12', 'tag4');

-- Tweet 13
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 13', 'user4', '2023-12-24 04:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('13', 'tag6'), ('13', 'tag8'), ('13', 'tag10');

-- Tweet 14
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 14', 'user5', '2023-12-24 05:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('14', 'tag1'), ('14', 'tag3'), ('14', 'tag5'), ('14', 'tag7');

-- Tweet 15
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 15', 'user1', '2023-12-24 06:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('15', 'tag9'), ('15', 'tag10');

-- Tweet 16
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 16', 'user2', '2023-12-24 07:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('16', 'tag2'), ('16', 'tag4'), ('16', 'tag6');

-- Tweet 17
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 17', 'user3', '2023-12-24 08:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('17', 'tag8'), ('17', 'tag10');

-- Tweet 18
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 18', 'user4', '2023-12-24 09:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('18', 'tag1'), ('18', 'tag3'), ('18', 'tag5');

-- Tweet 19
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 19', 'user5', '2023-12-24 10:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('19', 'tag7'), ('19', 'tag9');

-- Tweet 20
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 20', 'user1', '2023-12-24 11:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('20', 'tag2'), ('20', 'tag4'), ('20', 'tag6');

-- Tweet 21
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 21', 'user2', '2023-12-24 12:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('21', 'tag8'), ('21', 'tag10');

-- Tweet 22
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 22', 'user3', '2023-12-24 13:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('22', 'tag1'), ('22', 'tag3');

-- Tweet 23
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 23', 'user4', '2023-12-24 14:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('23', 'tag5'), ('23', 'tag7'), ('23', 'tag9');

-- Tweet 24
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 24', 'user5', '2023-12-24 15:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('24', 'tag2'), ('24', 'tag4'), ('24', 'tag6');

-- Tweet 25
INSERT INTO tweet_response (tweet_body, created_by, created_at)
VALUES ('Tweet 25', 'user1', '2023-12-24 16:00:00');

INSERT INTO tweet_response_hash_tags (tweet_response_tweet_id, hash_tags)
VALUES ('25', 'tag8'), ('25', 'tag10');
