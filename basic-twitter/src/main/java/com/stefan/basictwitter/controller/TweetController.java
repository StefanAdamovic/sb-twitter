package com.stefan.basictwitter.controller;

import com.stefan.basictwitter.model.PostTweetRequest;
import com.stefan.basictwitter.model.TweetResponse;
import com.stefan.basictwitter.model.TweetsPageResponse;
import com.stefan.basictwitter.service.TweetResponseService;
import jakarta.validation.constraints.Pattern;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("v1")
public class TweetController {

    private final TweetResponseService tweetResponseService;

    public TweetController(TweetResponseService tweetResponseService) {
        this.tweetResponseService = tweetResponseService;
    }

    @PostMapping("/tweets")
    public ResponseEntity<TweetResponse> createTweet(
            @RequestBody PostTweetRequest postTweetRequest,
            @RequestHeader("username") @Pattern(regexp = "^[a-zA-Z0-9_]{4,32}$") String username) {

        return new ResponseEntity<>(
                tweetResponseService.save(postTweetRequest, username), HttpStatus.CREATED);
    }

    @GetMapping("/tweets")
    public ResponseEntity<TweetsPageResponse> getTweets(
            @RequestParam(value = "hashTag", required = false) List<String> hashTags,
            @RequestParam(value = "username", required = false) List<String> usernames,
            @RequestParam(value = "limit", defaultValue = "50", required = false) Integer limit,
            @RequestParam(value = "offset", defaultValue = "0", required = false) Integer offset,
            @RequestHeader("username") @Pattern(regexp = "^[a-zA-Z0-9_]{4,32}$") String username) {

        if (hashTags == null && usernames == null)
            return new ResponseEntity<>(tweetResponseService.find(limit, offset), HttpStatus.OK);

        if (hashTags == null)
            return new ResponseEntity<>(tweetResponseService.find(limit, offset, usernames), HttpStatus.OK);

        if (usernames == null)
            return new ResponseEntity<>(tweetResponseService.find(hashTags, limit, offset), HttpStatus.OK);

        return new ResponseEntity<>(
                tweetResponseService.find(hashTags, limit, offset, usernames), HttpStatus.OK);

    }

    // TODO delete tweet
    @DeleteMapping("/tweets/{tweetId}")
    public ResponseEntity<TweetResponse> deleteTweet(
            @PathVariable long tweetId,
            @RequestHeader("username") @Pattern(regexp = "^[a-zA-Z0-9_]{4,32}$") String username) {

        return new ResponseEntity<>(tweetResponseService.delete(tweetId, username), HttpStatus.OK);
    }
}
