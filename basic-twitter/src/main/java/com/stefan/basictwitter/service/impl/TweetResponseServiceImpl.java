package com.stefan.basictwitter.service.impl;

import com.stefan.basictwitter.exception.ResourceNotFoundException;
import com.stefan.basictwitter.exception.UserTweetDeleteException;
import com.stefan.basictwitter.model.PostTweetRequest;
import com.stefan.basictwitter.model.TweetResponse;
import com.stefan.basictwitter.model.TweetsPageResponse;
import com.stefan.basictwitter.repository.TweetResponseRepository;
import com.stefan.basictwitter.service.TweetResponseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TweetResponseServiceImpl implements TweetResponseService {

    TweetResponseRepository tweetResponseRepository;

    @Autowired
    public TweetResponseServiceImpl(TweetResponseRepository tweetResponseRepository) {
        this.tweetResponseRepository = tweetResponseRepository;
    }

    @Override
    public TweetResponse save(PostTweetRequest postTweetRequest, String username) {
        TweetResponse tweetResponse = new TweetResponse(postTweetRequest, username);
        return tweetResponseRepository.save(tweetResponse);
    }

    @Override
    public TweetResponse delete(long tweetId, String username) {
        TweetResponse tweetResponse = tweetResponseRepository.findById(tweetId)
                .orElseThrow(() -> new ResourceNotFoundException("Resource not found."));

        if (tweetResponse.getCreatedBy().equals(username)) {
            tweetResponseRepository.deleteById(tweetId);
        } else throw new UserTweetDeleteException("Cant delete tweet of another user.");

        return tweetResponse;
    }

    @Override
    public TweetsPageResponse find(Integer limit, Integer offset) {
        TweetsPageResponse tweetsPageResponse = new TweetsPageResponse();

        List<TweetResponse> listOfTweets;

        listOfTweets = tweetResponseRepository.find(limit, offset);

        tweetsPageResponse.setTweetResponses(listOfTweets);

        tweetsPageResponse.setNextPage(setNextPage(null, null, limit, offset));

        return tweetsPageResponse;
    }

    @Override
    public TweetsPageResponse find(List<String> hashTags, Integer limit, Integer offset, List<String> usernames) {
        TweetsPageResponse tweetsPageResponse = new TweetsPageResponse();

        List<TweetResponse> listOfTweets;

        listOfTweets = tweetResponseRepository.find(hashTags, limit, offset, usernames);


        tweetsPageResponse.setTweetResponses(listOfTweets);

        tweetsPageResponse.setNextPage(setNextPage(hashTags, usernames, limit, offset));

        return tweetsPageResponse;
    }

    @Override
    public TweetsPageResponse find(Integer limit, Integer offset, List<String> usernames) {
        TweetsPageResponse tweetsPageResponse = new TweetsPageResponse();

        List<TweetResponse> listOfTweets;

        listOfTweets = tweetResponseRepository.find(limit, offset, usernames);


        tweetsPageResponse.setTweetResponses(listOfTweets);

        tweetsPageResponse.setNextPage(setNextPage(null, usernames, limit, offset));

        return tweetsPageResponse;
    }

    @Override
    public TweetsPageResponse find(List<String> hashTags, Integer limit, Integer offset) {
        TweetsPageResponse tweetsPageResponse = new TweetsPageResponse();

        List<TweetResponse> listOfTweets;

        listOfTweets = tweetResponseRepository.find(hashTags, limit, offset);


        tweetsPageResponse.setTweetResponses(listOfTweets);


        tweetsPageResponse.setNextPage(setNextPage(hashTags, null, limit, offset));

        return tweetsPageResponse;
    }


    private String setNextPage(List<String> hashTags, List<String> usernames, Integer limit, Integer offset) {

        StringBuilder nextPage = new StringBuilder("/v1/tweets?");

        if (!(hashTags == null)) {

            nextPage.append("hashTag=");
            for (String hashT : hashTags) {
                nextPage.append(",").append(hashT);
            }
        }
        if (!(usernames == null)) {
            nextPage.append("&username=");

            for (String u : usernames) {
                nextPage.append(",").append(u);
            }
        }

        nextPage.append("&limit=").append(limit).append("&offset=").append(limit + offset);

        return nextPage.toString();
    }


}
