package com.stefan.basictwitter.service;

import com.stefan.basictwitter.model.PostTweetRequest;
import com.stefan.basictwitter.model.TweetResponse;
import com.stefan.basictwitter.model.TweetsPageResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TweetResponseService {
    TweetResponse save(PostTweetRequest postTweetRequest, String username);

    TweetsPageResponse find(Integer limit, Integer offset);

    TweetsPageResponse find(List<String> hashTags, Integer limit, Integer offset, List<String> usernames);
    TweetsPageResponse find(List<String> hashTags, Integer limit, Integer offset);
    TweetsPageResponse find(Integer limit, Integer offset, List<String> usernames);

    TweetResponse delete(long tweetId, String username);
}
