package com.stefan.basictwitter.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TweetsPageResponse {

    private List<TweetResponse> tweetResponses;

    private String nextPage;
}
