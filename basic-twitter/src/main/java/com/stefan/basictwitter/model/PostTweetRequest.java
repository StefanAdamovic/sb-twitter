package com.stefan.basictwitter.model;

import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PostTweetRequest {

    @Size(max = 320)
    private String tweetBody;

    @Size(max = 5)
    private List<String> hashTags;
}
