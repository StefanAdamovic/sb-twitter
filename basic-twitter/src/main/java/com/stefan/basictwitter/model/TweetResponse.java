package com.stefan.basictwitter.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class TweetResponse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long tweetId;

    @Size(max = 320)
    @Column(name = "tweet_body")
    private String tweetBody;


    @Column(name = "hash_tags")
    @ElementCollection(targetClass = String.class, fetch = FetchType.EAGER)
    @Size(max = 5)
    private List<String> hashTags;

    @Column
    private String createdBy;

    @Column
    private LocalDateTime createdAt;

    public TweetResponse(PostTweetRequest postTweetRequest, String username) {
        this.tweetBody = postTweetRequest.getTweetBody();
        this.hashTags = postTweetRequest.getHashTags();
        this.createdAt = LocalDateTime.now();
        this.createdBy = username;
    }
}
