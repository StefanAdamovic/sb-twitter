package com.stefan.basictwitter.repository;

import com.stefan.basictwitter.model.TweetResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TweetResponseRepository extends JpaRepository<TweetResponse, Long> {


    @Query(value = "SELECT * FROM tweet_response ORDER BY created_at DESC LIMIT :lim OFFSET :off ", nativeQuery = true)
    List<TweetResponse> find(@Param("lim") Integer limit, @Param("off") Integer offset);


    @Query(value =
            "SELECT * FROM tweet_response tr JOIN tweet_response_hash_tags trh ON tr.tweet_id = trh.tweet_response_tweet_id " +
                    "WHERE trh.hash_tags IN (:hash_tags) " +
                    "ORDER BY tr.created_at DESC LIMIT :lim OFFSET :off"
            , nativeQuery = true)
    List<TweetResponse> find(
            @Param("hash_tags") List<String> hashTags, @Param("lim") Integer limit,
            @Param("off") Integer offset);

    @Query(value =
            "SELECT * FROM tweet_response tr WHERE tr.created_by IN (:created_by) " +
                    "ORDER BY tr.created_at DESC LIMIT :lim OFFSET :off"
            , nativeQuery = true)
    List<TweetResponse> find(
            @Param("lim") Integer limit,
            @Param("off") Integer offset,
            @Param("created_by") List<String> usernames);


    @Query(value =
            "SELECT * FROM tweet_response tr JOIN tweet_response_hash_tags trh ON tr.tweet_id = trh.tweet_response_tweet_id " +
                    "WHERE tr.created_by IN (:created_by) " +
                    "AND trh.hash_tags IN (:hash_tags) " +
                    "ORDER BY tr.created_at DESC LIMIT :lim OFFSET :off"
            , nativeQuery = true)
    List<TweetResponse> find(
            @Param("hash_tags") List<String> hashTags, @Param("lim") Integer limit,
            @Param("off") Integer offset,
            @Param("created_by") List<String> usernames);
}
