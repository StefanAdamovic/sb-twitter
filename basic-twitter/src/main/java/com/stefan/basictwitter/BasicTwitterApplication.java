package com.stefan.basictwitter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BasicTwitterApplication {

	public static void main(String[] args) {
		SpringApplication.run(BasicTwitterApplication.class, args);
	}

}
