package com.stefan.basictwitter.exception;

public class UserTweetDeleteException extends RuntimeException {

    public UserTweetDeleteException(String msg) {
        super(msg);
    }
}
