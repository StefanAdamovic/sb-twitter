package com.stefan.basictwitter.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.HandlerMethodValidationException;

@RestControllerAdvice
public class ControllerAdviceHandler {

    @ExceptionHandler(MissingRequestHeaderException.class)
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public ErrorMessage usernameMissingHeader(MissingRequestHeaderException ex) {
        return new ErrorMessage(
                HttpStatus.UNAUTHORIZED.value(), 103, ex.getMessage());
    }

    @ExceptionHandler(HandlerMethodValidationException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage usernamePatternNotMatch(HandlerMethodValidationException ex) {
        return new ErrorMessage(
                HttpStatus.BAD_REQUEST.value(), 103, ex.getMessage());
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorMessage resourceNotFound(ResourceNotFoundException ex) {
        return new ErrorMessage(
                HttpStatus.NOT_FOUND.value(), 103, ex.getMessage());
    }

    @ExceptionHandler(UserTweetDeleteException.class)
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    public ErrorMessage resourceNotFound(UserTweetDeleteException ex) {
        return new ErrorMessage(
                HttpStatus.FORBIDDEN.value(), 103, ex.getMessage());
    }

}
